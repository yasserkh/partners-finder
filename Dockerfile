FROM node:14

# create root application folder
WORKDIR /app

# copy configs to /app folder
COPY package*.json ./
COPY tsconfig.json ./

# copy source code to /app/src folder
COPY src /app/src
COPY data /app/data

# check files list
RUN ls -a

RUN npm run rmdir -- /app/dist

RUN npm install
RUN npm run build

EXPOSE 7777

CMD [ "node", "./dist/app.js" ]
import MathUtils from "../../src/utils/math-utils";
import Coordinate from "../../src/models/coordinate";


test("Should return Correct Distance in kilometeres", async () => {
    let firstCoordinate = new Coordinate(51.5144636, -0.142571);
    let secondCoordinate = new Coordinate(51.5136102, -0.08757919999993646);

    let distanceInKiloMeters = MathUtils.distanceInKiloMeters(firstCoordinate, secondCoordinate);
    expect(distanceInKiloMeters).toBe(3.8);
});
import Company from "../../../src/models/companies";
import Coordinate from "../../../src/models/coordinate";
import CompaniesRepository from "../../../src/repositories/file-source-repositories/companies-repository";

test("Should return Company Models", async () => {
    const companiesRepository = new CompaniesRepository();
    const companyModels = await companiesRepository.fetchAllPartnerCompanies();
    expect(companyModels).toHaveLength(17);
});

test("test fetchPartnerCompaniesInRange", async () => {
    const companiesRepository = new CompaniesRepository();
    const companyModels = await companiesRepository.fetchPartnerCompaniesInRange(new Coordinate(51.5144636, -0.142571), 10);
    //console.log(companyModels);
    expect(companyModels[0].company.id).toBe(4);
    expect(companyModels[1].company.id).toBe(13);
});

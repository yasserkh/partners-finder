import Coordinate from '../models/coordinate';

export default class MathUtils {
    public static distanceInKiloMeters(firstCoordinate: Coordinate, secondCoordinate: Coordinate) : number {
        let earthRadiusInKM = 6371;
        let lattitudeDifference = this.degreeToRadians(firstCoordinate.latitude - secondCoordinate.latitude);
        let longitudeDifference = this.degreeToRadians(firstCoordinate.longitude - secondCoordinate.longitude);

        let a = 
            Math.sin(lattitudeDifference/2) * Math.sin(lattitudeDifference/2) +
            Math.cos(this.degreeToRadians(firstCoordinate.latitude)) * Math.cos(this.degreeToRadians(firstCoordinate.latitude)) * 
            Math.sin(longitudeDifference/2) * Math.sin(longitudeDifference/2); 

        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        let d = earthRadiusInKM * c;

        return +d.toFixed(1);
    }

    public static degreeToRadians(degree: number) {
        return degree * (Math.PI/180);
    }
}

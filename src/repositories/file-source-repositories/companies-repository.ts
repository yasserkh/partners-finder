import Company from "../../models/companies";
import CompanyOfficeDistance from "../../models/company-office-distance";
import companyOfficeDistance from "../../models/company-office-distance";
import Coordinate from "../../models/coordinate";
import coordinate from "../../models/coordinate";
import MathUtils from "../../utils/math-utils";
import ICompniesRepository from "../interfaces/companies-repository";

const fs = require('fs');

export default class CompaniesRepository implements ICompniesRepository {
    async fetchPartnerCompaniesInRange(sourceCoordinate: Coordinate, rangeInKM: number): Promise<CompanyOfficeDistance[]> {
        let allCompanies = await this.fetchAllPartnerCompanies();

        let companyOfficeDistances = new Array<companyOfficeDistance>();

        allCompanies.forEach(company => {
            let companyOfficeDistance = this.findNearestOffice(sourceCoordinate, company);
            companyOfficeDistances.push(companyOfficeDistance);
        });

        companyOfficeDistances = companyOfficeDistances.filter(companyOfficeDistance => companyOfficeDistance.distanceInKM <= rangeInKM);

        // return companyOfficeDistances.sort(function compareFn(first, second) {
        //     return first.distanceInKM - second.distanceInKM;
        // });

        return companyOfficeDistances.sort((first, second) => first.company.organization.localeCompare(second.company.organization));
    }

    findNearestOffice(originCoordinate: Coordinate, company: Company) : CompanyOfficeDistance {
        let shortestDistance = Number.MAX_VALUE;

        let nearestOffice = company.offices[0];

        company.offices.forEach(office => {
            let distance = MathUtils.distanceInKiloMeters(originCoordinate, office.coordinate);
            if(distance < shortestDistance) {
                shortestDistance = distance;
                nearestOffice = office;
            }
        });

        return new CompanyOfficeDistance(company, shortestDistance, nearestOffice);
    }

    async fetchAllPartnerCompanies(): Promise<Company[]> {
        const path = require("path");
        let partnersRawData = fs.readFileSync(path.resolve(__dirname, '../../../data/partners.json'));
        let partnersJson = JSON.parse(partnersRawData);
        //console.log(partnersJson);
        
        let companies = Array<Company>();

        partnersJson.forEach(partnerJson => { 
            let company = Company.fromJson(partnerJson);
            companies.push(company);
        });

        return companies;
    }
}
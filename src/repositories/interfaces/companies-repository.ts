import Company from "../../models/companies";
import CompanyOfficeDistance from "../../models/company-office-distance";
import Coordinate from "../../models/coordinate";


export default interface ICompniesRepository {
    fetchPartnerCompaniesInRange(originCoordinate: Coordinate, rangeInKM: number) : Promise<Array<CompanyOfficeDistance>>;

    fetchAllPartnerCompanies() : Promise<Array<Company>>;
}
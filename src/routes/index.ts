import express from "express";
import PartnersController from "../controllers/partners";
import PingController from "../controllers/ping";
import Coordinate from "../models/coordinate";

const router = express.Router();

router.get("/ping", async (_req, res) => {
  const controller = new PingController();
  const response = await controller.getMessage();
  return res.send(response);
});

router.get("/fetch-partners-in-range", async (request, res) => {
    const controller = new PartnersController();

    let range = request.query.range ?? 0;
    const response = await controller.getPartnersInRange(new Coordinate(51.5144636, -0.142571), +range);
    return res.send(response);
});

export default router;
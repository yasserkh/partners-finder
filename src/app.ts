import express from 'express';
import Router from "./routes";
const cors = require('cors');

const app = express();
app.use(cors());
const port = 8080;
//const host = '0.0.0.0';

app.use(cors());
app.use(Router);


app.listen(port, () => {
  return console.log(`server is listening on ${port}`);
});
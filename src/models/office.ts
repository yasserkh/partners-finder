import Coordinate from './coordinate';

interface IOffice {
    location: string;
    address: string;
    coordinate: Coordinate;
}

export default class Office implements IOffice {
    location: string;
    address: string;
    coordinate: Coordinate;

    constructor(location: string, address: string, coordinate: Coordinate) { 
        this.location = location;
        this.address = address;
        this.coordinate = coordinate;
    }

    public static fromJson(json: JSON) : Office {
        let coordinates = Coordinate.fromString(json['coordinates']);
        let office = new Office(json['location'], json['address'], coordinates);
        return office;
    }
}
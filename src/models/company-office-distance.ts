import Company from './companies';
import Office from './office';

interface ICompanyOfficeDistance {
    company: Company;
    distanceInKM: number;
    nearestOffice: Office;
}

export default class CompanyOfficeDistance implements ICompanyOfficeDistance {
    company: Company;
    distanceInKM: number;
    nearestOffice: Office;

    constructor(company: Company, distanceInKM: number, nearestOffice: Office) { 
        this.company = company;
        this.distanceInKM = distanceInKM;
        this.nearestOffice = nearestOffice;
    }
}
interface ICoordinate {
    latitude: number;
    longitude: number;
}

export default class Coordinate implements ICoordinate {
    latitude: number;
    longitude: number;
    
    constructor(latitude: number, longitude: number) { 
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static fromString(coordinateString: string) : Coordinate {
        let coordinates = coordinateString.split(',');
        let coordinate = new Coordinate(+coordinates[0], +coordinates[1]);
        return coordinate;
    }
}
interface ICustomerLocation  {
    location: string;
}

export default class CustomerLocation implements ICustomerLocation {
    location: string;

    constructor(location: string) {
        this.location = location;
    }
}
import Office from "./office";
import CustomerLocation from "./customer-location";

interface ICompany {
    id: number;
    urlName: string;
    organization: string;
    customerLocations: Array<CustomerLocation>;
    willWorkRemotely: boolean;
    website: string;
    services: string;
    offices: Array<Office>;
}

export default class Company implements ICompany {
    id: number;
    urlName: string;
    organization: string;
    customerLocations: CustomerLocation[];
    willWorkRemotely: boolean;
    website: string;
    services: string;
    offices: Office[];
    
    constructor(id: number, urlName: string, organization: string, 
        customerLocations: CustomerLocation[], willWorkRemotely: boolean, 
        website: string, services: string, offices: Office[]) { 
        this.id = id;
        this.urlName = urlName;
        this.organization = organization;
        this.customerLocations = customerLocations;
        this.willWorkRemotely = willWorkRemotely;
        this.website = website;
        this.services = services;
        this.offices = offices;
    }

    public static fromJson(json: JSON) {
        let offices = new Array<Office>();
        json['offices'].forEach(officeJson => {
            offices.push(Office.fromJson(officeJson));
        });

        let customerLocations = new Array<CustomerLocation>();
        json['customerLocations'].split(',').forEach(cusomerLocation => {
            customerLocations.push(new CustomerLocation((cusomerLocation + '').trim()));
        });

        let company = new Company(
            +json['id'],
            json['urlName'],
            json['organization'],
            customerLocations,
            json['willWorkRemotely'],
            json['website'],
            json['services'],
            offices
        );

        return company;
    }
}
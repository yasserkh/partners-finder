import CompanyOfficeDistance from "../models/company-office-distance";
import Coordinate from "../models/coordinate";
import Office from "../models/office";
import CompaniesRepository from "../repositories/file-source-repositories/companies-repository";

interface Partner {
    id: number,
    organization: string,
    website: string,
    distance: number,
    office: Office
}

interface PartnersResponse {
    companyOfficeDistances: Array<Partner>;
}
  
export default class PartnersController {
    public async getPartnersInRange(originCoordinate: Coordinate, rangeInKM: number): Promise<Partner[]> {
        await new Promise(resolve => setTimeout(resolve, 200));

        let companiesRepository = new CompaniesRepository();
        let compnaieOfficeDistances = await companiesRepository.fetchPartnerCompaniesInRange(originCoordinate, rangeInKM);
        let partners = new Array();
        compnaieOfficeDistances.forEach(element => {
            partners.push({
                id: element.company.id,
                organization: element.company.organization,
                website: element.company.website,
                distance: element.distanceInKM,
                office: element.nearestOffice
            });
        });
        return partners;
    }
}